from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import uic
import sys
import hashlib
import datetime
from datetime import datetime
import keras
from tensorflow.keras.models import load_model
from PyQt5.uic import loadUi
from cv2 import cv2
import mysql.connector
sys.path.append('../insightface/deploy')
sys.path.append('../insightface/src/common')
from mtcnn.mtcnn import MTCNN
from imutils import paths
import face_preprocess
import numpy as np
import argparse
import face_model
import os
import pickle
import time
import dlib
import faces_embedding
import train_softmax
from gtts import gTTS 
import playsound
from pathlib import Path
import requests
url_post = 'http://dev-zent.zentsoft.com/api/attendance/checkin'
configue_path_all_file="D:\\Downloads\\Face-Recognition-with-InsightFace-master\\src\\"
# Define distance function
cap=cv2.VideoCapture(0)

screen_res = QApplication(sys.argv)
rect = screen_res.primaryScreen().availableGeometry()
width=rect.width()
height=rect.height()
print(width)

def findCosineDistance(vector1, vector2):
    """
    Calculate cosine distance between two vector
    """
    vec1 = vector1.flatten()
    vec2 = vector2.flatten()

    a = np.dot(vec1.T, vec2)
    b = np.dot(vec1.T, vec1)
    c = np.dot(vec2.T, vec2)
    return 1 - (a/(np.sqrt(b)*np.sqrt(c)))

def CosineSimilarity(test_vec, source_vecs):
    """
    Verify the similarity of one vector to group vectors of one class
    """
    cos_dist = 0
    for source_vec in source_vecs:
        cos_dist += findCosineDistance(test_vec, source_vec)
    return cos_dist/len(source_vecs)
detector = MTCNN()
def showDialog(a,b):
    msgBox = QMessageBox()
    msgBox.setGeometry(570,320,300,100)
    msgBox.setIcon(QMessageBox.Information)
    msgBox.setText(a)
    msgBox.setWindowTitle(b)
    msgBox.setStandardButtons(QMessageBox.Ok)
    returnValue = msgBox.exec()
    if returnValue == QMessageBox.Ok:
        print('OK clicked')
class UI_diem_danh(QMainWindow):
    def __init__(self):
        super(UI_diem_danh, self).__init__()
        self.id_student=""
        uic.loadUi(configue_path_all_file+'diem_danh.ui', self)
        self.list_staff.setGeometry(0,0,0.25*width,height)
        self.cam.setGeometry(0.25*width,0,0.75*width,height)
        self.showMaximized()
        self.thread1=QThreadPool()
        self.thread2=QThreadPool()
        self.thread1.start(self.load_camera)
        self.thread2.start(self.proccessing)

    #search từ CSDL
    def load_camera(self):
        if not cap.isOpened():
            print("Cannot open camera")
            exit()
        while True:
            # Capture frame-by-frame                                            
            ret, frame = cap.read()
            # if frame is read correctly ret is True
            if not ret:
                print("Can't receive frame (stream end?). Exiting ...")
                break               
            gray = cv2.cvtColor(frame, 1)
            gray=cv2.resize(frame,(int(0.75*width),height),interpolation= cv2.INTER_AREA)
            self.displayImgage(gray,1)
            cv2.waitKey(0)
            if cv2.waitKey(1) == ord('a'):
                break       
   #Lấy ảnh từ camera
    def proccessing(self):
        os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
        os.environ["CUDA_VISIBLE_DEVICES"]="0,3"
        ap = argparse.ArgumentParser()

        ap.add_argument("--mymodel", default="outputs/my_model.h5",
            help="Path to recognizer model")
        ap.add_argument("--le", default="outputs/le.pickle",
            help="Path to label encoder")
        ap.add_argument("--embeddings", default="outputs/embeddings.pickle",
            help='Path to embeddings')
        ap.add_argument("--video-out", default="../datasets/videos_output/stream_test.mp4",
            help='Path to output video')


        ap.add_argument('--image-size', default='112,112', help='')
        ap.add_argument('--model', default='../insightface/models/model-y1-test2/model,0', help='path to load model.')
        ap.add_argument('--ga-model', default='', help='path to load model.')
        ap.add_argument('--gpu', default=0, type=int, help='gpu id')
        ap.add_argument('--det', default=0, type=int, help='mtcnn option, 1 means using R+O, 0 means detect from begining')
        ap.add_argument('--flip', default=0, type=int, help='whether do lr flip aug')
        ap.add_argument('--threshold', default=1.24, type=float, help='ver dist threshold')

        args = ap.parse_args()

        # Load embeddings and labels
        data = pickle.loads(open(args.embeddings, "rb").read())
        le = pickle.loads(open(args.le, "rb").read())

        embeddings = np.array(data['embeddings'])
        labels = le.fit_transform(data['names'])

        # Initialize detector
        detector = MTCNN()

        # Initialize faces embedding model
        embedding_model =face_model.FaceModel(args)

        # Load the classifier model
        model = load_model('outputs/my_model.h5')


        # Initialize some useful arguments
        cosine_threshold = 0.8
        proba_threshold = 0.85
        comparing_num = 5
        trackers = []
        texts = []
        frames = 0
        kq=np.zeros(len(embeddings), dtype=int)

        frame_width = int(cap.get(3))
        frame_height = int(cap.get(4))
        save_width = 1000
        save_height = 720
        video_out = cv2.VideoWriter(args.video_out, cv2.VideoWriter_fourcc('M','J','P','G'), 30, (save_width,save_height))
        x1,x2=0,5
        text_recent= "Unknown"
        dem=0
        while True:

            ret, frame = cap.read()
            frames += 1
            rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame = cv2.resize(frame, (save_width, save_height))
            gray = cv2.cvtColor(frame, 1)
            gray=cv2.resize(frame,(int(0.75*width),height),interpolation= cv2.INTER_AREA)

            if frames%3 == 0 :
                trackers = []
                texts = []
            
                bboxes = detector.detect_faces(frame)
                if len(bboxes) != 0:
                    for bboxe in bboxes:
                        bbox = bboxe['box']
                        bbox = np.array([bbox[0], bbox[1], bbox[0]+bbox[2], bbox[1]+bbox[3]])
                        landmarks = bboxe['keypoints']
                        landmarks = np.array([landmarks["left_eye"][0], landmarks["right_eye"][0], landmarks["nose"][0], landmarks["mouth_left"][0], landmarks["mouth_right"][0],
                                            landmarks["left_eye"][1], landmarks["right_eye"][1], landmarks["nose"][1], landmarks["mouth_left"][1], landmarks["mouth_right"][1]])
                        landmarks = landmarks.reshape((2,5)).T
                        nimg = face_preprocess.preprocess(frame, bbox, landmarks, image_size='112,112')
                        nimg = cv2.cvtColor(nimg, cv2.COLOR_BGR2RGB)
                        nimg = np.transpose(nimg, (2,0,1))
                        embedding = embedding_model.get_feature(nimg).reshape(1,-1)


                        # Predict class
                        preds = model.predict(embedding)
                        preds = preds.flatten()
                        # Get the highest accuracy embedded vector
                        j = np.argmax(preds)
                        proba = preds[j]
                        # Compare this vector to source class vectors to verify it is actual belong to this class
                        match_class_idx = (labels == j)
                        match_class_idx = np.where(match_class_idx)[0]
                        selected_idx = np.random.choice(match_class_idx, comparing_num)
                        compare_embeddings = embeddings[selected_idx]
                        kq[j]+=1
                        # Calculate cosine similarity
                        cos_similarity = CosineSimilarity(embedding, compare_embeddings)
                        if cos_similarity < cosine_threshold and proba > proba_threshold:
                            name = le.classes_[j]
                            text_recent = "{}".format(name)
                            print("Recognized: {} <{:.2f}>".format(name, proba*100))
                        
                        # Start tracking
                        tracker = dlib.correlation_tracker()
                        rect = dlib.rectangle(bbox[0], bbox[1], bbox[2], bbox[3])
                        tracker.start_track(rgb, rect)
                        trackers.append(tracker)
                        texts.append(text_recent)

            else:
                for tracker, text in zip(trackers,texts):
                    pos = tracker.get_position()

                    # unpack the position object
                    startX = int(pos.left())
                    startY = int(pos.top())
                    endX = int(pos.right())
                    endY = int(pos.bottom())

            for i in range(len(embeddings)):
                if kq[i]>=5:
                    kq=np.zeros(len(embeddings), dtype=int)     
                    mydb = mysql.connector.connect(
                    host="localhost",
                    user="root",
                    password="admin",
                    database="diem_danh"
                        )
                    mycursor = mydb.cursor()
                    a=0
                    #truy van thong tin nguoi diem danh
                    if a==0:
                        sql_check="SELECT * FROM users where user_id=%(user_id)s;"
                        data_check = {
                            'user_id':text_recent,
                        }
                        mycursor.execute(sql_check, data_check)
                        myresult=mycursor.fetchall()
                        print(myresult)
                        #luu thong tin nguoi vua checkin
                        user_type=myresult[0][3]
                        user_checkin=myresult[0][1]

                        
                        named_tuple = time.localtime() # lấy struct_time
                        time_now=time.strftime("%Y-%m-%d %H:%M:%S")
                        date = time.strftime("%Y-%m-%d", named_tuple)
                        timec=time.strftime("%H:%M:%S",named_tuple)

                        #truy van xem nguoi do da diem danh gan day ko.
                        sql_last_check="SELECT MAX(time_checkin) FROM checkin WHERE id=%(id)s and date_checkin=%(date_checkin)s ; "
                        data_last_check={
                            'id':text_recent,
                            'date_checkin':date,
                        }
                        mycursor.execute(sql_last_check,data_last_check)
                        myresult=mycursor.fetchall()
                        print(myresult)
                        delta_time=0
                        if myresult[0][0] is None:
                            delta_time=0
                        else:                   
                            #convert time tu mysql sang time python
                            last_time_checkin = datetime.strptime(str(myresult[0][0]), '%H:%M:%S')
                            format1 = '%H:%M:%S'
                            delta_time=datetime.strptime(timec, format1) - datetime.strptime(str(myresult[0][0]), '%H:%M:%S')
                            #convert time to timestamp
                            delta_time=delta_time.total_seconds()

                        #neu chua checkin trong ngay hoac sau 1h chua checkin lan nao thi checkin
                        if myresult[0][0] is None or delta_time>=3600:
                            # thêm checkin và DB
                            
                            sql_checkin="insert into checkin(id ,date_checkin,time_checkin) values(%(id)s,%(date_checkin)s,%(time_checkin)s);"
                            data_checkin = {
                            'id':text_recent,
                            'date_checkin':date,
                            'time_checkin':timec,  
                            }
                            mycursor.execute(sql_checkin,data_checkin)
                            mydb.commit() 
                            mydb.close()
    
                            #gửi request lên API
                            string_token="stag@devimd.edu.vn"
                            public_key=text_recent+time_now+str(user_type)+string_token
                            print(public_key)
                            token= hashlib.md5(public_key.encode('utf-8')).hexdigest()
                            print(token)
                            global url_post
                            datas = {
                                    'id':text_recent,
                                    'time':time_now, 
                                    'type':user_type,
                                    'token':token
                                    } 

                            x = requests.post(url_post, data = datas)
                            print(x.text)
                            dem=dem+1
                            staff=""
                            try:
                                staff=str(time_now) + " "+ str(user_checkin)
                                app_speak="xin chào" + str(user_checkin)
                                tts=gTTS(text=app_speak,lang='vi')
                                path_voice=configue_path_all_file+str(text_recent)+".mp3"
                                my_file = Path(path_voice)
                                if my_file.is_file():
                                    playsound.playsound(path_voice)
                                    self.list_staff.appendPlainText(staff)
                                else:
                                    tts.save(path_voice)
                                    playsound.playsound(path_voice)
                                    self.list_staff.appendPlainText(staff)
                            except:
                                print("fail")
                            staff=""
                    else:
                        print("fail---")
                    if dem%20==0:
                        self.list_staff.setPlainText("")

                    # print(myresult)
                    text_recent="Unknown"

                    break
        
        cap.release()
        cv2.destroyAllWindows()
        showDialog("Bạn đã lấy dữ liệu ảnh thành công", "Successful")
    def displayImgage(self,img,window=1):
        qformat=QImage.Format_Indexed8
        if len(img.shape)==3:
            if(img.shape[2])==4:
                qformat=QImage.Format_RGB888
            else:
                qformat=QImage.Format_RGB888
        img=QImage(img,img.shape[1],img.shape[0],qformat)
        img=img.rgbSwapped()
        self.cam.setPixmap(QPixmap.fromImage(img))
app = QApplication(sys.argv)
window = UI_diem_danh()
sys.exit(app.exec_())

from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QTextEdit,QMessageBox,QCompleter
from PyQt5 import uic
import threading
import sys
from PyQt5.QtGui import QImage,QPixmap
import datetime
from PyQt5.uic import loadUi
from PyQt5.QtCore import pyqtSlot
from cv2 import cv2
import mysql.connector
sys.path.append('../insightface/deploy')
sys.path.append('../insightface/src/common')
from mtcnn.mtcnn import MTCNN
from imutils import paths
import face_preprocess
import numpy as np
import argparse
import os
import faces_embedding
import train_softmax
import requests
import requests_cache
import hashlib 
configue_string_url="https://stag.zent.vn"
configue_path_all_file="D:\\Downloads\\Face-Recognition-with-InsightFace-master\\src\\"
path_image_data="D:\\Downloads\\Face-Recognition-with-InsightFace-master\\datasets\\train"
detector = MTCNN()
def showDialog(a,b):
    msgBox = QMessageBox()
    msgBox.setGeometry(570,320,300,100)
    msgBox.setIcon(QMessageBox.Information)
    msgBox.setText(a)
    msgBox.setWindowTitle(b)
    msgBox.setStandardButtons(QMessageBox.Ok)


    returnValue = msgBox.exec()
    if returnValue == QMessageBox.Ok:
        print('OK clicked')
class UI(QMainWindow):
    def __init__(self):
        super(UI, self).__init__()
        global configue_path_all_file
        uic.loadUi(configue_path_all_file+'my_app.ui', self)
        self.type_frame=1
        # find the widgets in the xml file
        self.account=""
        self.b_search.clicked.connect(self.search)
        self.b_add.clicked.connect(self.add)
        self.b_train.clicked.connect(self.train)
        # self.l_edit_search.textEdited.connect(self.call_api)
        self.rbtn1.toggled.connect(self.onRadioBtn1)
        self.rbtn2.toggled.connect(self.onRadioBtn2)
        self.show()
    #search từ CSDL
    def call_api(self):        
        # self.rbtn1.toggled.connect(self.onRadioBtn1)
        # self.rbtn2.toggled.connect(self.onRadioBtn2)
        # self.b_search.clicked.connect(self.search)
        self.account=self.l_edit_search.text()
        types=self.type_frame
        if types==0:
            self.label.setText("Thông Tin Giảng Viên")
            self.label_2.setText("Mã Giảng Viên")
            self.label_3.setText("Tên Giảng Viên")
        else:
            self.label.setText("Thông Tin Học Viên")
            self.label_2.setText("Mã Học Viên")
            self.label_3.setText("Tên Học Viên")

        name=self.account

        string_token="stag@devimd.edu.vn"
        public_key=name+str(types)+string_token
        print(public_key)
        token= hashlib.md5(public_key.encode('utf-8')).hexdigest()
        print(token)

        url = configue_string_url+"/api/students/search?q={0}&type={1}&token={2}".format(name,types,token)
        print(url)
        response_dict = requests.get(url).json()
        status_request=response_dict["status"]
        print(status_request)
        list_completer=[]
        completer=QCompleter()
        if(status_request["code"]==200):
            data_user=response_dict["data"]
            for i in data_user:
                list_completer.append(str(i["name"]))
                        # +": "+str(i["account"]
            print(list_completer)
            completer = QCompleter(list_completer)
            self.l_edit_search.setCompleter(completer)

                    # user=data_user[0]
                    # account_user=user["account"]
                    # name_user=user["name"]
                    # self.account=account_user
                    # self.id_hocvien.setText(account_user)
                    # self.name.setText(name_user)     
                # except:
                #     self.id_hocvien.setText("")
                #     self.name.setText("")
                    
        else:
            self.id_hocvien.setText("")
            self.name.setText("")


    def search(self):
        self.account=self.l_edit_search.text()
        self.rbtn1.toggled.connect(self.onRadioBtn1)
        self.rbtn2.toggled.connect(self.onRadioBtn2)
        types=self.type_frame
        if types==0:
            self.label.setText("Thông Tin Giảng Viên")
            self.label_2.setText("Mã Giảng Viên")
            self.label_3.setText("Tên Giảng Viên")
        else:
            self.label.setText("Thông Tin Học Viên")
            self.label_2.setText("Mã Học Viên")
            self.label_3.setText("Tên Học Viên")

        name=self.account
        string_token="stag@devimd.edu.vn"
        public_key=name+str(types)+string_token
        print(public_key)
        token= hashlib.md5(public_key.encode('utf-8')).hexdigest()
        print(token)

        url = configue_string_url+"/api/students/search?q={0}&type={1}&token={2}".format(name,types,token)
        print(url)
        response_dict = requests.get(url).json()
        status_request=response_dict["status"]
        print(status_request)
        if(status_request["code"]==200):
            try:
                data_user=response_dict["data"]
                print(data_user)
                user=data_user[0]
                account_user=user["account"]
                name_user=user["name"]
                self.account=account_user
                self.id_hocvien.setText(account_user)
                self.name.setText(name_user)

                try:
                    mydb = mysql.connector.connect(
                            host="localhost",
                            user="root",
                            password="admin",
                            database="diem_danh"
                        )
                    mycursor = mydb.cursor()
                    sql_insert="insert into users(account,name,type) values(%(account)s,%(name)s,%(type)s);"
                    data_insert = {
                        'account':account_user,
                        'name':name_user,
                        'type':self.type_frame,
                        }
                    mycursor.execute(sql_insert,data_insert)
                                                
                    mydb.commit() 
                    mydb.close()
                except:
                   showDialog("người này đã được đăng kí,ban muốn đăng kí lại","Thông báo")
            except:
                self.id_hocvien.setText("")
                self.name.setText("")
                showDialog("không có người này trong Database","Thông báo")
        else:
            self.id_hocvien.setText("")
            self.name.setText("")
            showDialog("bạn nhập không hợp lệ","Thông báo")
    def onRadioBtn1(self):
        rbtn=self.sender()
        if rbtn.isChecked():
            self.type_frame=0
    def onRadioBtn2(self):
        rbtn=self.sender()
        if rbtn.isChecked():
            self.type_frame=1



    #Lấy ảnh từ camera
    def add(self):
        self.b_add.setText("Đang Thêm Ảnh ...")
        path_image_data="D:\\Downloads\\Face-Recognition-with-InsightFace-master\\datasets\\train"

        path_image_data=path_image_data+"\\"+self.account
        faces = 0
        frames = 0
        max_faces = 40       #  set number face 
        max_bbox = np.zeros(4)
        cap = cv2.VideoCapture(0)#if you want open file video to read,you will add path into () || ex:cap = cv2.VideoCapture(D:/nhu/imagie)
        if not cap.isOpened():
            print("Cannot open camera")
            exit()
        while faces < max_faces:
            # Capture frame-by-frame                                            
            ret, frame = cap.read()
            # if frame is read correctly ret is True
            if not ret:
                print("Can't receive frame (stream end?). Exiting ...")
                break           
            frames += 1
            bboxes = detector.detect_faces(frame)    
            # Our operations on the frame come here
            gray = cv2.cvtColor(frame, 1)
            gray=cv2.resize(frame,(500,500))

            if len(bboxes) != 0:
            # Get only the biggest face
                max_area = 0
                for bboxe in bboxes:
                    bbox = bboxe["box"]
                    bbox = np.array([bbox[0], bbox[1], bbox[0]+bbox[2], bbox[1]+bbox[3]])
                    keypoints = bboxe["keypoints"]
                    area = (bbox[2]-bbox[0])*(bbox[3]-bbox[1])
                    if area > max_area:
                        max_bbox = bbox
                        landmarks = keypoints
                        max_area = area
                max_bbox = max_bbox[0:4]
                    # get each of 3 frames
                if frames%3 == 0:
                    # convert to face_preprocess.preprocess input
                    landmarks = np.array([landmarks["left_eye"][0], landmarks["right_eye"][0], landmarks["nose"][0], landmarks["mouth_left"][0], landmarks["mouth_right"][0],
                                        landmarks["left_eye"][1], landmarks["right_eye"][1], landmarks["nose"][1], landmarks["mouth_left"][1], landmarks["mouth_right"][1]])
                    landmarks = landmarks.reshape((2,5)).T
                    nimg = face_preprocess.preprocess(frame, max_bbox, landmarks, image_size='112,112')
                    if not(os.path.exists(path_image_data)):
                        os.makedirs(path_image_data)
                    cv2.imwrite(os.path.join(path_image_data, "{}.jpg".format(faces+1)), nimg)
                    cv2.rectangle(frame, (max_bbox[0], max_bbox[1]), (max_bbox[2], max_bbox[3]), (255, 0, 0), 2)
                    print("[INFO] {} faces detected".format(faces+1))
                    faces += 1
            # Display the resulting frame
            self.displayImgage(gray,1)
            cv2.waitKey()
        # When everything done, release the capture
        cap.release()
        cv2.destroyAllWindows()
        self.b_add.setText("Thêm Ảnh")
        showDialog("Bạn đã lấy dữ liệu ảnh thành công", "Successful")
    #train models
    def train(self): 
        self.b_train.setText("Đang Huấn luyện ...")
        faces_embedding.compress_data()
        train_softmax.train_s() 
        showDialog("Huấn luyện đã hoàn tất","Hoàn Thành")
        print("train success")
        self.b_train.setText("Huấn luyện mô hình")

    def displayImgage(self,img,window=1):
        qformat=QImage.Format_Indexed8
        if len(img.shape)==3:
            if(img.shape[2])==4:
                qformat=QImage.Format_RGB888
            else:
                qformat=QImage.Format_RGB888
        img=QImage(img,img.shape[1],img.shape[0],qformat)
        img=img.rgbSwapped()
        self.capture.setPixmap(QPixmap.fromImage(img))
app = QApplication(sys.argv)
window = UI()
sys.exit(app.exec_())

import multiprocessing
from multiprocessing import Process
from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QTextEdit,QMessageBox,QCompleter
from PyQt5 import uic
import sys
configue_path_all_file="D:\\Downloads\\Face-Recognition-with-InsightFace-master\\src\\"

class UI(QMainWindow):
    def __init__(self):
        super(UI, self).__init__()
        global configue_path_all_file
        uic.loadUi(configue_path_all_file+'testui.ui', self)
        self.show()
    
    
if __name__=="__main__":
    app = QApplication(sys.argv)
    window = UI()
    sys.exit(app.exec_())

    print("We're done")
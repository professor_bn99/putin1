from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QTextEdit,QMessageBox
from PyQt5 import uic
import sys
from PyQt5.QtGui import QImage,QPixmap
import datetime
from PyQt5.uic import loadUi
from PyQt5.QtCore import pyqtSlot
from cv2 import cv2
import mysql.connector
sys.path.append('../insightface/deploy')
sys.path.append('../insightface/src/common')
from mtcnn.mtcnn import MTCNN
from imutils import paths
import face_preprocess
import numpy as np
import argparse
import os
import faces_embedding
import train_softmax
detector = MTCNN()
def showDialog(a,b):
    msgBox = QMessageBox()
    msgBox.setGeometry(570,320,300,100)
    msgBox.setIcon(QMessageBox.Information)
    msgBox.setText(a)
    msgBox.setWindowTitle(b)
    msgBox.setStandardButtons(QMessageBox.Ok)


    returnValue = msgBox.exec()
    if returnValue == QMessageBox.Ok:
        print('OK clicked')
class UI_diem_danh(QMainWindow):
    def __init__(self):
        super(UI_diem_danh, self).__init__()

        uic.loadUi('D:\\FaceRecognize\\Face-Recognition-with-InsightFace-master\\src\\diem_danh.ui', self)
 
        # find the widgets in the xml file

        # self.b_start.clicked.connect(self.start)

        self.show()
        self.start()
    #search từ CSDL

   #Lấy ảnh từ camera
    def start(self):

        frames = 0
    #  set number face 
        max_bbox = np.zeros(4)
        cap = cv2.VideoCapture(0)#if you want open file video to read,you will add path into () || ex:cap = cv2.VideoCapture(D:/nhu/imagie)
        if not cap.isOpened():
            print("Cannot open camera")
            exit()
        while True:
            # Capture frame-by-frame                                            
            ret, frame = cap.read()
            # if frame is read correctly ret is True
            if not ret:
                print("Can't receive frame (stream end?). Exiting ...")
                break           
            frames += 1
            bboxes = detector.detect_faces(frame)    
            # Our operations on the frame come here
            gray = cv2.cvtColor(frame, 1)
            gray=cv2.resize(frame,(500,500))
            self.displayImgage(gray,1)
            cv2.waitKey()
        # When everything done, release the capture
        cap.release()
        cv2.destroyAllWindows()
        showDialog("Bạn đã lấy dữ liệu ảnh thành công", "Successful")
    def displayImgage(self,img,window=1):
        qformat=QImage.Format_Indexed8
        if len(img.shape)==3:
            if(img.shape[2])==4:
                qformat=QImage.Format_RGB888
            else:
                qformat=QImage.Format_RGB888
        img=QImage(img,img.shape[1],img.shape[0],qformat)
        img=img.rgbSwapped()
        self.cam.setPixmap(QPixmap.fromImage(img))
app = QApplication(sys.argv)
window = UI_diem_danh()
sys.exit(app.exec_())

from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from PyQt5 import uic
import sys
import datetime
from PyQt5.uic import loadUi
from cv2 import cv2
import mysql.connector
sys.path.append('../insightface/deploy')
sys.path.append('../insightface/src/common')
from mtcnn.mtcnn import MTCNN
from imutils import paths
import face_preprocess
import numpy as np
import argparse
import os
import faces_embedding
import train_softmax
import requests
import requests_cache
import hashlib 
configue_string_url="http://dev-zent.zentsoft.com"
configue_path_all_file="D:\\Downloads\\Face-Recognition-with-InsightFace-master\\src\\"
path_image_data="D:\\Downloads\\Face-Recognition-with-InsightFace-master\\datasets\\train"
string_token="stag@devimd.edu.vn"
detector = MTCNN()

list_completer=[]
class UI(QMainWindow):
    def __init__(self):
        super(UI, self).__init__()
        global configue_path_all_file
        uic.loadUi(configue_path_all_file+'my_app.ui', self)
        self.type_frame=1
        self.l_api1.hide()
        self.l_api2.hide()
        self.l_api3.hide()
        self.l_api4.hide()
        self.l_api5.hide()
        self.l_api1.clicked.connect(self.search_api1)
        self.l_api2.clicked.connect(self.search_api2)
        self.l_api3.clicked.connect(self.search_api3)
        self.l_api4.clicked.connect(self.search_api4)
        self.l_api5.clicked.connect(self.search_api5)
        # find the widgets in the xml file
        self.account=""
        self.name=""
        self.mail=""
        self.list_id=[]
        self.list_name=[]
        self.user_id=""
        self.list_user_id=[]
        self.list_mail=[]
        self.b_search.clicked.connect(self.search)
        self.b_add.clicked.connect(self.add)
        self.b_train.clicked.connect(self.train)
        self.l_edit_search.textEdited.connect(self.call_api)
        self.rbtn1.toggled.connect(self.onRadioBtn1)
        self.rbtn2.toggled.connect(self.onRadioBtn2)
        self.show()
    #search từ CSDL
    def showDialog(self,a,b):
        self.msgBox = QMessageBox()
        self.msgBox.setGeometry(570,320,300,100)
        self.msgBox.setIcon(QMessageBox.Information)
        self.msgBox.setText(a)
        self.msgBox.setWindowTitle(b)
        self.msgBox.setStandardButtons(QMessageBox.Ok)

        returnValue = self.msgBox.exec()
        if returnValue == QMessageBox.Ok:
            print('OK clicked')
    def load_label(self):
        types=self.type_frame
        if types==0:
            self.label.setText("Thông Tin Giảng Viên")
            self.label_2.setText("Mã Giảng Viên")
            self.label_3.setText("Tên Giảng Viên")
        else:
            self.label.setText("Thông Tin Học Viên")
            self.label_2.setText("Mã Học Viên")
            self.label_3.setText("Tên Học Viên")
        
    def call_api(self):    
        self.l_api1.setText("")
        self.l_api2.setText("")
        self.l_api3.setText("")
        self.l_api4.setText("")
        self.l_api5.setText("")
        self.l_api1.hide()
        self.l_api2.hide()
        self.l_api3.hide()
        self.l_api4.hide()
        self.l_api5.hide()
        # self.rbtn1.toggled.connect(self.onRadioBtn1)
        # self.rbtn2.toggled.connect(self.onRadioBtn2)
        # self.b_search.clicked.connect(self.search)
        self.account=self.l_edit_search.text()
        self.load_label()
        name=self.account

        public_key=name+str(self.type_frame)+string_token
        print(public_key)
        token= hashlib.md5(public_key.encode('utf-8')).hexdigest()
        print(token)

        url = configue_string_url+"/api/students/search?q={0}&type={1}&token={2}".format(name,self.type_frame,token)
        print(url)
        try:
            response_dict = requests.get(url).json()
            status_request=response_dict["status"]

            print(status_request)
            list_completer=[]
            self.list_id=[]
            self.list_name=[]
            self.list_user_id=[]
            self.list_mail=[]
            if(status_request["code"]==200):
                data_user=response_dict["data"]
                print(data_user)
                try:
                    for i in data_user:
                        list_completer.append(str(i["name"]) +": "+str(i["account"]))
                        self.list_id.append(str(i["account"]))
                        self.list_name.append(str(i["name"]))
                        self.list_user_id.append(str(i["id"]))
                        self.list_mail.append(str(i['email']))
                except:
                    print("api tra ve null")
                print(list_completer)
                if len(list_completer)==0:
                    self.l_api1.hide()
                    self.l_api2.hide()
                    self.l_api3.hide()
                    self.l_api4.hide()
                    self.l_api5.hide()               
                elif len(list_completer)==1:
                    self.l_api1.show()
                    self.l_api1.setText(list_completer[0])

                elif len(list_completer)==2:
                    self.l_api1.show()
                    self.l_api1.setText(list_completer[0])
                    self.l_api2.show()
                    self.l_api2.setText(list_completer[1])
                elif len(list_completer)==3:
                    self.l_api1.show()
                    self.l_api1.setText(list_completer[0])
                    self.l_api2.show()
                    self.l_api2.setText(list_completer[1])
                    self.l_api3.show()
                    self.l_api3.setText(list_completer[2])

                elif len(list_completer)==4:
                    self.l_api1.show()
                    self.l_api1.setText(list_completer[0])
                    self.l_api2.show()
                    self.l_api2.setText(list_completer[1])
                    self.l_api3.show()
                    self.l_api3.setText(list_completer[2])
                    self.l_api4.show()
                    self.l_api4.setText(list_completer[3])

                else:
                    self.l_api1.show()
                    self.l_api1.setText(list_completer[0])
                    self.l_api2.show()
                    self.l_api2.setText(list_completer[1])
                    self.l_api3.show()
                    self.l_api3.setText(list_completer[2])
                    self.l_api4.show()
                    self.l_api4.setText(list_completer[3])
                    self.l_api5.show()
                    self.l_api5.setText(list_completer[4])
               
            else:
                self.id_hocvien.setText("")
                self.l_name.setText("")
                self.l_mail.setText("")
        except:
            print("false")
    def api_setText(self):
        self.id_hocvien.setText(self.account)
        self.l_name.setText(self.name)
        self.l_mail.setText(self.mail)
        self.l_edit_search.setText(self.name)
        self.insert_staff()
        self.hide_view()
    def search_api1(self):
        self.account=self.list_id[0]
        self.name=self.list_name[0]
        self.user_id=self.list_user_id[0]
        self.mail=self.list_mail[0]
        self.api_setText()
    def search_api2(self):
        self.account=self.list_id[1]
        self.name=self.list_name[1]
        self.user_id=self.list_user_id[1]
        self.mail=self.list_mail[1]
        self.api_setText()
    def search_api3(self):
        self.account=self.list_id[2]
        self.name=self.list_name[2]
        self.user_id=self.list_user_id[2]
        self.mail=self.list_mail[2]
        self.api_setText()
    def search_api4(self):
        self.account=self.list_id[3]
        self.name=self.list_name[3]
        self.user_id=self.list_user_id[3]
        self.mail=self.list_mail[3]
        self.api_setText()
    def search_api5(self):
        self.account=self.list_id[4]
        self.name=self.list_name[4]
        self.user_id=self.list_user_id[4]
        self.mail=self.list_mail[4]
        self.api_setText()
    def hide_view(self):
        self.l_api1.hide()
        self.l_api2.hide()
        self.l_api3.hide()
        self.l_api4.hide()
        self.l_api5.hide()         
    def insert_staff(self):
        try:
            mydb = mysql.connector.connect(
                host="localhost",
                user="root",
                password="admin",
                database="diem_danh"
            )
            mycursor = mydb.cursor()
            sql_insert="insert into users(account,name,type,user_id) values(%(account)s,%(name)s,%(type)s,%(user_id)s);"
            data_insert = {
                    'account':self.account,
                    'name':self.name,
                    'type':self.type_frame,
                    'user_id':self.user_id,
                     }
            mycursor.execute(sql_insert,data_insert)                                    
            mydb.commit() 
            mydb.close()
        except:
            print("da them ")   
    def search(self):
        self.rbtn1.toggled.connect(self.onRadioBtn1)
        self.rbtn2.toggled.connect(self.onRadioBtn2)
        self.load_label()


        name=self.account
        public_key=name+str(self.type_frame)+string_token
        print(public_key)
        token= hashlib.md5(public_key.encode('utf-8')).hexdigest()
        print(token)

        url = configue_string_url+"/api/students/search?q={0}&type={1}&token={2}".format(name,self.type_frame,token)
        print(url)
        response_dict = requests.get(url).json()
        status_request=response_dict["status"]
        print(status_request)
        if(status_request["code"]==200):
            try:
                data_user=response_dict["data"]
                print(data_user)
                user=data_user[0]
                account_user=user["account"]
                name_user=user["name"]
                self.account=account_user
                self.id_hocvien.setText(account_user)
                self.l_name.setText(name_user)

                try:
                    mydb = mysql.connector.connect(
                            host="localhost",
                            user="root",
                            password="admin",
                            database="diem_danh"
                        )
                    mycursor = mydb.cursor()
                    sql_insert="insert into users(account,name,type) values(%(account)s,%(name)s,%(type)s);"
                    data_insert = {
                        'account':account_user,
                        'name':name_user,
                        'type':self.type_frame,
                        }
                    mycursor.execute(sql_insert,data_insert)
                                                
                    mydb.commit() 
                    mydb.close()
                except:
                   self.showDialog("người này đã có trong Database","Thông báo")
            except:
                self.id_hocvien.setText("")
                self.l_name.setText("")
                self.showDialog("không có người này trong Database","Thông báo")
        else:
            self.id_hocvien.setText("")
            self.l_name.setText("")
            self.showDialog("bạn nhập không hợp lệ","Thông báo")
    def onRadioBtn1(self):
        rbtn=self.sender()
        if rbtn.isChecked():
            self.type_frame=0
            self.call_api()
    def onRadioBtn2(self):
        rbtn=self.sender()
        if rbtn.isChecked():
            self.type_frame=1
            self.call_api

    #Lấy ảnh từ camera
    def add(self):
        if self.account !="":
            self.b_add.setText("Đang Thêm Ảnh ...")
            path_image_data="D:\\Downloads\\Face-Recognition-with-InsightFace-master\\datasets\\train"

            path_image_data=path_image_data+"\\"+self.user_id
            faces = 0
            frames = 0
            max_faces = 40       #  set number face 
            max_bbox = np.zeros(4)
            cap = cv2.VideoCapture(0)#if you want open file video to read,you will add path into () || ex:cap = cv2.VideoCapture(D:/nhu/imagie)
            if not cap.isOpened():
                print("Cannot open camera")
                exit()
            while faces < max_faces:
                # Capture frame-by-frame                                            
                ret, frame = cap.read()
                # if frame is read correctly ret is True
                if not ret:
                    print("Can't receive frame (stream end?). Exiting ...")
                    break           
                frames += 1
                bboxes = detector.detect_faces(frame)    
                # Our operations on the frame come here
                gray = cv2.cvtColor(frame, 1)
                gray=cv2.resize(frame,(500,500))

                if len(bboxes) != 0:
                # Get only the biggest face
                    max_area = 0
                    for bboxe in bboxes:
                        bbox = bboxe["box"]
                        bbox = np.array([bbox[0], bbox[1], bbox[0]+bbox[2], bbox[1]+bbox[3]])
                        keypoints = bboxe["keypoints"]
                        area = (bbox[2]-bbox[0])*(bbox[3]-bbox[1])
                        if area > max_area:
                            max_bbox = bbox
                            landmarks = keypoints
                            max_area = area
                    max_bbox = max_bbox[0:4]
                        # get each of 3 frames
                    if frames%3 == 0:
                        # convert to face_preprocess.preprocess input
                        landmarks = np.array([landmarks["left_eye"][0], landmarks["right_eye"][0], landmarks["nose"][0], landmarks["mouth_left"][0], landmarks["mouth_right"][0],
                                            landmarks["left_eye"][1], landmarks["right_eye"][1], landmarks["nose"][1], landmarks["mouth_left"][1], landmarks["mouth_right"][1]])
                        landmarks = landmarks.reshape((2,5)).T
                        nimg = face_preprocess.preprocess(frame, max_bbox, landmarks, image_size='112,112')
                        if not(os.path.exists(path_image_data)):
                            os.makedirs(path_image_data)
                        cv2.imwrite(os.path.join(path_image_data, "{}.jpg".format(faces+1)), nimg)
                        cv2.rectangle(frame, (max_bbox[0], max_bbox[1]), (max_bbox[2], max_bbox[3]), (255, 0, 0), 2)
                        print("[INFO] {} faces detected".format(faces+1))
                        faces += 1
                # Display the resulting frame
                self.displayImgage(gray,1)
                cv2.waitKey()
            # When everything done, release the capture
            cap.release()
            cv2.destroyAllWindows()
            self.b_add.setText("Thêm Ảnh")
            self.showDialog("Bạn đã lấy dữ liệu ảnh thành công", "Successful")
        else:
            self.showDialog("Bạn chưa nhập thông tin"," ")
    #train models
    def setTextTrain(self):
        self.b_train.setText("Đang Huấn luyện ...")

    def Training_model(self):
        faces_embedding.compress_data()
        train_softmax.train_s() 
        self.b_train.setText("Huấn luyện mô hình")

        print("train success")

    def train(self): 
        self.thread1=QThreadPool()
        self.thread2=QThreadPool()
        self.thread1.start(self.setTextTrain)
        self.thread2.start(self.Training_model) 
    def displayImgage(self,img,window=1):
        qformat=QImage.Format_Indexed8
        if len(img.shape)==3:
            if(img.shape[2])==4:
                qformat=QImage.Format_RGB888
            else:
                qformat=QImage.Format_RGB888
        img=QImage(img,img.shape[1],img.shape[0],qformat)
        img=img.rgbSwapped()
        self.capture.setPixmap(QPixmap.fromImage(img))
app = QApplication(sys.argv)
window = UI()
sys.exit(app.exec_())
